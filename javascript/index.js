/*global process */
var net = require('net'),
    JSONStream = require('JSONStream'),
    main = require('./lib/main');

var serverHost = process.argv[2],
    serverPort = process.argv[3],
    botName = process.argv[4],
    botKey = process.argv[5];

console.log('I\'m', botName, 'and connect to', serverHost + ':' + serverPort);

function send(json) {
  client.write(JSON.stringify(json));
  return client.write('\n');
}

var client = net.connect(serverPort, serverHost, function() {
  main.init({name: botName, key: botKey}, send);
});
client.setNoDelay(true);

var jsonStream = client.pipe(JSONStream.parse());

jsonStream.on('data', main.serverMessage);
jsonStream.on('error', function() {
  return console.log('disconnected');
});
