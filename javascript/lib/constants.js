exports.INFINITY = 1e9;           // Infinite brake distance, when no need to brake =)
exports.MAX_SPEED = 50.0;
exports.MAX_ANGLE = 60;               // Max angle of car drift
exports.MAX_EBRAKE_TICKS = 15;        // Number of ticks required for emergency braking
exports.DRIFT_COEFF = 14.0;           // Drift coefficient: keimola, usa, germany, france, elaeintarha
exports.BASE_SPEED = 1.635;           // Speed after 10 ticks
exports.MASS = 296;                   // Car mass
exports.DEFAULT_FR_ACCEL = -0.3;
exports.ADJUSTMENT = 0.007;
