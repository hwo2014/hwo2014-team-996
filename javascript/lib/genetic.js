var _ = require('lodash'),
    util = require('./util'),
    Log = require('./log'),
    track = require('./track'),
    race = require('./new-world'),
    Const = require('./constants');

var STEPS = 450,
    DATA_LENGTH = 150,
    FIT_THRESHOLD = 0.06,
    MAX_THRESHOLD = 0.12;

// Public data object for genetic algorithm
exports.calibration = {
  populationSize: 150,
  population: undefined,
  data: [],
  step: 0,

  baseA: 0.52,
  baseKf: 0.3,
  basea0: 0.00125,
  basea1: 0.1,

  intA: 0.6,
  intKf: 0.3,
  inta0: 0.001,
  inta1: 0.005,

  pMutation: 0.35,
  aMutation: 18
};

process.on('message', function(m) {
  if (m.type === 'track') { track.init(m.data); }
  if (m.type === 'physics') { race.physics = m.data; }
  if (m.type === 'data') {
    process.nextTick(function() { collectData(m.data); });
  }
  if (m.type === 'reset') {
    // Shit can happen: reset on crash
    var cal = exports.calibration;
    if (!cal.population) {
      if (cal.data.length > 3*DATA_LENGTH/5) {
        process.nextTick(_initCalibration);
      } else {
        exports.calibration.data = [];
      }
    }
  }
});

function collectData(data) {
  if (exports.calibration.population) { return; }

  var cal = exports.calibration;

  if (!data.car.angle && cal.data.length > 5) { cal.data.shift(); }
  exports.calibration.data.push({
    car: data.car,
    throttle: data.throttle,
    angle: data.car.angle
  });

  if (cal.data.length > DATA_LENGTH) {
    process.send({type: 'data-collected'});
    _initCalibration();
  }
}

function _initCalibration() {
  var cal = exports.calibration;
  cal.population = _initialPopulation(
      cal.populationSize*2,
      {min: cal.baseA - cal.intA/2, max: cal.baseA + cal.intA/2},
      {min: cal.baseKf - cal.intKf/2, max: cal.baseKf + cal.intKf/2},
      {min: cal.basea0 - cal.inta0/2, max: cal.basea0 + cal.inta0/2},
      {min: cal.basea1 - cal.inta1/2, max: cal.basea1 + cal.inta1/2});
  _calibratePhysics(cal.population, track);
}

function _calibratePhysics(population, track) {
  var lastSlope = 1,
      slope,
      flat = 1;

  while (exports.calibration.step < STEPS) {
    var size = exports.calibration.populationSize,
        pop = exports.calibration.population,
        car0 = exports.calibration.data[0].car,
        fit = 0,
        bumpMutation = false,
        children = [], i;


    // Calculate fitness function for population
    for (i=0; i<pop.length; i++) {
      if (!pop[i].fit) {
        fit = Math.abs(race.fitness(car0, exports.calibration.data, track, pop[i]));
        pop[i].fit = fit;
      }
    }

    var best = _.min(pop, 'fit');
    if (!_.isEqual(exports.calibration.best, best)) {
      console.log(JSON.stringify(best));
      if (exports.calibration.best) {
        slope = (exports.calibration.best.fit - best.fit) / flat;
        if (slope < lastSlope) {
          bumpMutation = true;
        }
        lastSlope = slope;
      }
      exports.calibration.best = best;
      flat = 1;
    } else {
      flat++;
      if (flat > 7) {
        bumpMutation = true;
        flat = 1;
      }
    }

    if ((best.fit <= FIT_THRESHOLD && exports.calibration.step > STEPS / 2) || exports.calibration.step === STEPS-1) {
      best.angle = best.fit <= FIT_THRESHOLD
          ? 59.5 : (best.fit <= MAX_THRESHOLD ? 58.5 : 57.5);

      console.log('Finished on step: ' + exports.calibration.step);
      race.fitness(car0, exports.calibration.data, track, best, true);
      exports.calibration.step = STEPS;
      process.send({type: 'calibrated', data: best});
      return;
    }

    pop = _select(pop);
    children = _generate(pop, bumpMutation);
    pop.push.apply(pop, children);

    exports.calibration.population = pop;
    exports.calibration.step++;
  }
}

function _initialPopulation(size, rngA, rngK, rnga0, rnga1) {
  var pop = new Array(size);
  for (var i=0; i<size; i++) {
    pop[i] = {
      A: _.random(rngA.min, rngA.max, true),
      kf: _.random(rngK.min, rngK.max, true),
      a0: _.random(rnga0.min, rnga0.max, true),
      a1: _.random(rnga1.min, rnga1.max, true),
      fit: 0  // Fitness function value
    };
  }
  return pop;
}

function _select(population) {
  population = _.sortBy(population, 'fit');
  var size = exports.calibration.populationSize,
      eliteSize = Math.round(size / 10),
      result = population.splice(0, eliteSize),
      inds;

  // Tournament selection
  while (result.length < size) {
    inds = _.sample(population, 2);
    result.push(inds[0].fit < inds[1].fit ? inds[0] : inds[1]);
  }

  // New population
  return result;
}

function _generate(population, bumpMutation) {
  var cal = exports.calibration,
      children = [],
      inds,
      am = cal.aMutation,
      pm = bumpMutation ? 1 : cal.pMutation;

  while (children.length < cal.populationSize) {
    inds = _.sample(population, 2);
    if (_.isEqual(inds[0], inds[1])) { continue; }

    children.push({
      A: _mutate(_recombinate(inds[0].A, inds[1].A), cal.intA, pm, am),
      kf: _mutate(_recombinate(inds[0].kf, inds[1].kf), cal.intKf, pm, am),
      a0: _mutate(_recombinate(inds[0].a0, inds[1].a0), cal.inta0, pm, am),
      a1: _mutate(_recombinate(inds[0].a1, inds[1].a1), cal.inta1, pm, am)
    });
  }
  return children;
}

function _recombinate(gene1, gene2) {
  return gene1 + _.random(-0.25, 1.25) * (gene2 - gene1);
}

function _mutate(gene, interval, prob, factor) {
  if (Math.random() < prob) { return gene; }
  return gene + _p(0.5, 1, -1) * 0.5 * interval * _mutationStep(factor);
}

function _mutationStep(factor) {
  var sum = 0, i;
  for(i=0; i<factor; i++) {
    sum += _p(1/factor, 1, 0) / (1 << i);
  }
  return sum;
}

/**
 * Distance between 2 chromosomes, by type:
 * genotypic (truthy) or phenotypic (falsy)
 */
function _distance(ch1, ch2, type) {
  return type
      ? Math.abs(ch2.fit - ch1.fit)
      : Math.sqrt((ch2.A - ch1.A)*(ch2.A - ch1.A) + (ch2.kf - ch1.kf)*(ch2.kf - ch1.kf));
}

/**
 * Shortcut for finding a value with defined probability
 */
function _p(prob, win, lose) {
  return Math.random() < prob ? win : lose;
}
