var LOG_PATH = '../../hwo2014-logs/';
var fs = require('fs');
var time = Date.now();

exports.logTxt = function(type, message, ext) {
  ext = ext || 'txt';
  fs.appendFile(LOG_PATH + type + '_' + time + '.' + ext, message + '\n', function(err) { /* NOP */ });
}

exports.logCsv = function(type, params) {
  fs.appendFile(LOG_PATH + type + '_' + time + '.csv', params.join(',') + '\n', function(err) { /* NOP */ });
}
