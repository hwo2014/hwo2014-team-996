// Libraries
var _ = require('lodash'),
    Log = require('./log'),
    Const= require('./constants'),
    track = require('./track'),
    race = require('./new-world'),
    util = require('./util');

var fork = require('child_process').fork,
    worker = fork(__dirname + '/genetic');

// Flags
var devMode = false;

// world models
var botId, carsData, cars, position, maxLaps, switchIndex, currentThrottle = 0, currentTick = 0,
    car, simCar;

// Misc variables
var send;

/**
 * Race raceStatus
 */
var raceStatus = {
  crashed: false,

  activeTurbo: undefined,
  availableTurbo: undefined,
  requestedTurbo: undefined
};

worker.on('message', function(m) {
  if (m.type === 'data-collected') { race.dataCollected = true; }
  if (m.type === 'calibrated') {
    race.physics.A = m.data.A;
    race.physics.kf = m.data.kf;
    race.physics.a0 = m.data.a0;
    race.physics.a1 = m.data.a1;
    race.physics.MAX_ANGLE = m.data.angle;

    console.log('Finally calibrated on tick: ' + currentTick + ' for a=' + m.data.angle);
    simCar = _.clone(car);
  }
});

function createRace(track) {
  send({
    msgType: altBot() ? 'joinRace' : 'createRace',
    data: {
      botId: botId,
      trackName: track,
      password: 'fubar',
      carCount: 2
    }
  });
}

exports.init = function(bot, sendCallback) {
  botId = bot;
  send = sendCallback;

  if (devMode) {
    createRace('keimola');   //'elaeintarha'
  } else {
    send({
      msgType: 'join',
      data: botId
    });
  }
};

exports.serverMessage = function(data) {
  switch (data.msgType) {
    case 'carPositions':
      if (data.gameTick) {
        // cars = updateCars(botId, data.data, cars);
        driveCar(data);
      }
      break;
    case 'crash':
      if (isMe(data)) {
        raceStatus.crashed = true;
        race.markCrash(car);
        car.speed = 0;

        // If crashed when calibrating angle
        if (!race.dataCollected) { worker.send({type: 'reset'}); }
        console.log('crash: ' + JSON.stringify(data.data));
      }
      break;
    case 'spawn':
      if (isMe(data)) {
        raceStatus.crashed = false;
        console.log('spawn');
      }
      break;
    case 'gameInit':
      track.init(data.data.race);
      worker.send({type: 'track', data: data.data.race});

      carsData = data.data.race.cars;
      maxLaps = data.data.race.raceSession.laps;
      break;
    case 'yourCar':
      if (car) { car.id = data.data; }
      break;
    case 'lapFinished':
      switchIndex = undefined;
      if (data.data.car.name === botId.name) {
        console.log(JSON.stringify(data.data.lapTime));
      }
      break;
    case 'turboAvailable':
      if (!raceStatus.crashed && !raceStatus.availableTurbo) {
        raceStatus.availableTurbo = data.data;
        console.log(JSON.stringify(data));
      }
      break;
    case 'turboStart':
      if (isMe(data)) {
        console.log('banzai!');
        raceStatus.activeTurbo = _.clone(raceStatus.availableTurbo);
        raceStatus.activeTurbo.startTick = data.gameTick;
        raceStatus.availableTurbo = undefined;
        raceStatus.requestedTurbo = undefined;
      }
      break;
    case 'turboEnd':
      if (isMe(data)) {
        raceStatus.activeTurbo = undefined;
      }
      break;
    case 'gameStart':
      send({msgType: 'throttle', data: 0, gameTick: data.gameTick});
      break;
  }

  if (data.msgType !== 'carPositions') { console.log(data.gameTick + ': ' + data.msgType); }
};

/**
 * MAIN function of car operation
 */
function driveCar(data) {
  var __tstart = process.hrtime(), tick = data.gameTick;

  var position = findCar(data.data, botId),
      prevCar = car || race.createCar(tick, position),
      throttle = 1,
      mTicks = 0,
      prevThrottle = currentThrottle;

  // WTF tests
  if (!raceStatus.crashed) {
    mTicks = util.missingTicks(currentTick, tick, position);
    currentTick = tick;
  }

  if (!car) {
    car = race.createCar(tick, position);
  } else {
    car = race.updateCar(tick, track, car, position, raceStatus.activeTurbo);
  }

  var ws = checkWrongSpeed(car, prevCar);
  if (ws) { console.log(tick + ': ERROR: Wrong speed! Delta: ' + ws); }

  throttle = race.calibrated() ? race.getThrottle2(tick, car, track, raceStatus.activeTurbo) : 1;
  if (mTicks && mTicks < 400) { throttle = 0; }

  // Send command as early as possible
  if (command(tick, car, throttle)) { currentThrottle = throttle; }

  race.calibrateSpeed(tick, prevThrottle, car, updateWorkerPhysics);
  worker.send({type: 'data', data: {car: car, tick: tick, throttle: prevThrottle}});

  simCar = race.calibrated() && !raceStatus.crashed && car.speed > 0
      ? race.updateSimCar(track, simCar || car, prevThrottle * race.turboFactor(prevCar.tick, raceStatus.activeTurbo), {})
      : car;

  if (race.calibrated() && !race.dataCollected) { checkAngle(car, simCar, track); }

  if (devMode) {
    var params = race.carToLog(car);
    params.push('simulator: ');
    params.push.apply(params, race.carToLog(simCar || car));
    Log.logCsv('cars', params);

    Log.logCsv('data', [
      car.pieceIndex,
      car.endLaneIndex,
      track.pieceLength(car.pieceIndex, car.startLaneIndex, car.endLaneIndex),
      track.pieceRadius(car.pieceIndex, car.startLaneIndex, car.endLaneIndex),
      track.pieceAngle(car.pieceIndex, car.startLaneIndex, car.endLaneIndex, car.inPieceDistance),
      car.speed,
      throttle,
      raceStatus.activeTurbo ? raceStatus.activeTurbo.turboFactor : 1.0,
      position.angle,
      race.physics.A,
      util.benchmark(__tstart)
    ]);
  }
}

/**
 * Sends the command to the server. Returns true, if throttle command is send.
 */
function command(tick, car, throttle) {
  var index = car.pieceIndex,
      command = {msgType: 'throttle', data: throttle},
      switchTo = track.switchDirection(index, car.endLaneIndex);

  if (race.calibrated() && race.dataCollected
      && raceStatus.availableTurbo
      // && index === track.turboIndex
      && race.canUseTurbo(tick, car, track, raceStatus.availableTurbo)
      && !raceStatus.requestedTurbo) {
    raceStatus.requestedTurbo = tick;
    command = {msgType: 'turbo', data: 'banzai!'};
    console.log(tick + ': piece: ' + index + '; ' + JSON.stringify(command));
  } else if (race.calibrated() && race.dataCollected && switchTo) {
    // Avoid sending multiple switch commands
    if (switchIndex !== index && track.track.pieces[index+1] && track.track.pieces[index+1].switch) {
      switchIndex = index;
      command = {msgType: 'switchLane', data: switchTo < 0 ? 'Left' : 'Right'};
      console.log(tick + ': ' + 'on ' + index + ' ' + JSON.stringify(command));
    }
  }
  command.gameTick = tick;
  send(command);
  return command.msgType === 'throttle';
}

function updateWorkerPhysics(data) {
  console.log(JSON.stringify(data));
  worker.send({type: 'physics', data: data});
}

/**
 * Let the car start drifting ASAP
 */
function checkAngle(real, sim, track) {
  if (sim.angle
      && !real.angle && race.physics.A > 0.52
      && track.pieceRadius(real.pieceIndex, real.startLaneIndex, real.endLaneIndex) < 100) {
    race.physics.A -= 0.02;
    console.log(race.physics.A);
  }
}

function checkWrongSpeed (car, prevCar) {
  if (raceStatus.crashed || car.tick < 6) { return 0; }

  var refCar = race.updateSimCar(track,
      _.clone(prevCar),
      currentThrottle * race.turboFactor(prevCar.tick, raceStatus.activeTurbo), {});

  return Math.abs(car.speed - refCar.speed) > 1e-6
      ? car.speed - refCar.speed : 0;
}

/***************************************************************************************************
 Deal with other cars here
 **************************************************************************************************/
function isMe(data) {
  return data.data.name === botId.name;
}

function altBot() {
  return botId.name !== 'che';
}

function interval(first, second) {
  return first.piecePosition.inPieceDistance
      - first.backLength
      - second.piecePosition.inPieceDistance,
      - second.frontLength;
}

function updateCars(positions, cars) {
  var res = _.chain(positions)
      .filter(positions, function(car) { car.id.name !== botId.name; })
      .map(function(car) {
        var old = findCar(cars, car.id),
            data = findCar(carsData, car.id);

        car.frontLength = data.dimensions.guideFlagPosition;
        car.backLength = data.dimensions.length - data.dimensions.guideFlagPosition;
        car.speed = calcSpeed(old, car) || 0;
        car.a = car.speed - (old && old.speed || 0);
        return car;
      });
  return res.value();
}

function findCar(cars, id) {
  return _.find(cars, function(car) { return car.id.name === id.name; });
}

/**
 * Calculates car speed in ipd/tick (in-piece-distance per tick). Undefined when changing pieces.
 */
function calcSpeed(oldPos, newPos) {
  return oldPos && oldPos.piecePosition.pieceIndex === newPos.piecePosition.pieceIndex
      ? newPos.piecePosition.inPieceDistance - oldPos.piecePosition.inPieceDistance
      : undefined;
}
