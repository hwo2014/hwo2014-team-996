var _ = require('lodash'),
    util = require('./util'),
    Log = require('./log'),
    Const = require('./constants');

var data = [], speedData = [],
    PREDICTION_TIME = 65,
    TURBO_TIME = 16;

exports.dataCollected = false;

// Some track constants
exports.physics = {
  MAX_ANGLE: 30,
  A: 0.65,
  a0: 0.00125,
  a1: 0.1,
  kf: 0.3,

  kv: 0.1,
  mass: 5.0,

  speedCalibrated: 0,
  angleCalibrated: 0
};

exports.calibrated = function() {
  return exports.physics.speedCalibrated && exports.physics.angleCalibrated > 0;
};

exports.calibrateSpeed = function(tick, throttle, car, callback) {
  if (exports.physics.speedCalibrated) { return; }

  if (car.speed > 0) { speedData.push(car.speed); }
  console.log(tick + '   ' + JSON.stringify(speedData));

  if (speedData.length === 3) {
    var kv = (speedData[0]*2 - speedData[1]) / (speedData[0]*speedData[0]) * throttle,
        kt = throttle / kv;

    exports.physics.mass = -kv / Math.log((speedData[2] - kt) / (speedData[1] - kt));
    exports.physics.kv = kv;
    exports.physics.speedCalibrated = tick;
    exports.physics.angleCalibrated = tick;
    callback && callback(exports.physics);
  }
};

exports.speed = function(speed0, throttle, tNum) {
  var ph = exports.physics;
  return (speed0 - throttle/ph.kv) * Math.exp(-ph.kv * tNum / ph.mass) + throttle / ph.kv;
};

exports.createCar = function(tick, position) {
  position = position || {};
  return {
    a: 0,
    speed: 0,
    angle: 0,
    angleA: 0,
    angleSpeed: 0,
    turboFactor: 1,
    lapCount: 0,

    pieceIndex: position.piecePosition.pieceIndex || 0,
    inPieceDistance: position.piecePosition.inPieceDistance || 0,
    startLaneIndex: position.piecePosition.lane.startLaneIndex || 0,
    endLaneIndex: position.piecePosition.lane.endLaneIndex || 0,
    tick: tick || 0
  };
};

/**
 * Returns car object based on an given car object and new car position
 */
exports.updateCar = function(tick, track, car, position, turbo) {
  var speed = position.piecePosition.inPieceDistance - car.inPieceDistance;

  // Edge cases for speed calculation: from piece to piece
  if (car.pieceIndex !== position.piecePosition.pieceIndex) {

    // Changed lane in the previous piece - speed approximation would be the best guess...
    if (car.startLaneIndex !== car.endLaneIndex) {
      speed = car.speed + car.a;
    } else {
      speed += track.pieceLength(car.pieceIndex, car.endLaneIndex, car.endLaneIndex);
    }
  }

  var angleSpeed = position.angle - car.angle;

  return {
    a: speed - car.speed,
    angle: position.angle,
    angleSpeed: angleSpeed,
    angleA: angleSpeed - car.angleSpeed,
    speed: speed,
    turboFactor: turbo ? turbo.turboFactor : 1.0,
    pieceIndex: position.piecePosition.pieceIndex,
    inPieceDistance: position.piecePosition.inPieceDistance,
    startLaneIndex: position.piecePosition.lane.startLaneIndex,
    endLaneIndex: position.piecePosition.lane.endLaneIndex,
    tick: tick,
    lapCount: car.lapCount + (position.piecePosition.pieceIndex < car.pieceIndex ? 1 : 0)
  };
};

/**
 * Update car simulation model
 */
exports.updateSimCar = function(track, car, throttle, options) {
  var A = (options.physics || exports.physics).A,
      a0 = (options.physics || exports.physics).a0,
      a1 = (options.physics || exports.physics).a1,
      kf = (options.physics || exports.physics).kf;

  // When calibrating angle use speed and position data from an actual car at that step!
  if (options.calibrationMode) {
    car.pieceIndex = options.car.pieceIndex;
    car.inPieceDistance = options.car.inPieceDistance;
    car.speed = options.car.speed;
    car.a = options.car.a;
    car.startLaneIndex = options.car.startLaneIndex;
    car.endLaneIndex = options.car.endLaneIndex;
  }

  var index = car.pieceIndex,
      piece = track.track.pieces[index],
      radius = track.pieceRadius(index, car.startLaneIndex, car.endLaneIndex),
      angle0 = -a0 * car.speed * car.angle - a1 * car.angleSpeed,
      torque = 0, dir;

  if (!piece.length && car.speed > (kf/A) * Math.sqrt(radius)) {
    dir = track.pieceAngle(piece, car.startLaneIndex, car.endLaneIndex, car.inPieceDistance) > 0 ? 1 : -1;
    torque = dir * (A / Math.sqrt(radius) * car.speed * car.speed - kf * car.speed);
  }

  car.angleA = angle0 + torque;
  car.angleSpeed += car.angleA;
  car.angle += car.angleSpeed;

  var speed = exports.speed(car.speed, throttle, 1);

  car.a = speed - car.speed;
  car.speed = speed;
  car.inPieceDistance += car.speed;

  if (car.inPieceDistance > track.pieceLength(index, car.startLaneIndex, car.endLaneIndex)) {
    car.inPieceDistance -= track.pieceLength(index, car.startLaneIndex, car.endLaneIndex);
    car.pieceIndex = (index + 1) % track.length;

    if (car.pieceIndex === 0) { car.lapCount++; }

    car.startLaneIndex = car.endLaneIndex;
    if (!options.calibrationMode) {
      car.endLaneIndex = car.startLaneIndex + (track.track.pieces[car.pieceIndex].switch
          ? track.switchDirection(index, car.startLaneIndex) : 0);
    }

    // See if next tick will be skipped due to lane switch or requesting turbo
    if (options.predictionMode && _shouldAddTick(track, car)) {
      car.tick++;
      exports.updateSimCar(track, car, throttle, options);
    }
  }

  car.tick++;
  return car;
};

exports.getThrottle = function(tick, car0, track, turbo) {
  var CRIT_ANGLE = exports.physics.MAX_ANGLE,
      WARN_ANGLE = CRIT_ANGLE - 7,
      MIN_THR = 1/16,
      throttle = 1,
      maxAngle = 0,
      tStep = 1,
      tick = 0,
      car;

  while (tStep > MIN_THR) {
    car = exports.updateSimCar(track, _.clone(car0),
        exports.turboFactor(tick + 1, turbo) * throttle, {predictionMode: true});

    tick = 0;
    maxAngle = 0;
    while (Math.abs(car.angle) < CRIT_ANGLE && tick < PREDICTION_TIME) {
      exports.updateSimCar(track, car, 0, {predictionMode: true});
      maxAngle = Math.abs(car.angle) > maxAngle ? Math.abs(car.angle) : maxAngle;
      tick++;
    }

    // No crash, we're good
    if (tick === PREDICTION_TIME && throttle === 1) {
      return maxAngle > WARN_ANGLE ? 0.1 * (10 - maxAngle + WARN_ANGLE) : 1;
    }

    throttle = throttle + tStep * (tick < PREDICTION_TIME ? -1 : 1);
    tStep = tStep / 2;
  }

  return throttle <= MIN_THR ? 0 : throttle;
};

exports.getThrottle2 = function(tick, car0, track, turbo) {
  var CRIT_ANGLE = exports.physics.MAX_ANGLE,
      WARN_ANGLE = CRIT_ANGLE - 7,
      maxAngle = 0,
      tick = 0;

  var car = exports.updateSimCar(track, _.clone(car0), exports.turboFactor(tick + 1, turbo), {predictionMode: true});

  while (Math.abs(car.angle) <= CRIT_ANGLE && tick < PREDICTION_TIME) {
    exports.updateSimCar(track, car, 0, {predictionMode: true});
    maxAngle = Math.abs(car.angle) > maxAngle ? Math.abs(car.angle) : maxAngle;
    tick++;

    if (exports.crashMarks[car.pieceIndex]) {
      CRIT_ANGLE = exports.physics.MAX_ANGLE - 7;
      WARN_ANGLE = CRIT_ANGLE - 7;
    }
  }

  maxAngle = Math.min(maxAngle, CRIT_ANGLE);

  // No crash, we're good
  if (tick === PREDICTION_TIME) {
    return maxAngle > WARN_ANGLE ? 0.14 * (7 - maxAngle + WARN_ANGLE) : 1;
  }
  return 0;
};

exports.canUseTurbo = function(tick, car0, track, turbo) {
  if (car0.angle * car0.angleSpeed > 0) { return false; }
  if (car0.pieceIndex + 1 === track.turboIndex ||
      car0.pieceIndex === track.turboIndex) { return true; }

  var car = exports.updateSimCar(track, _.clone(car0), exports.turboFactor(tick + 1, turbo), {predictionMode: true}),
      throttle;

  for (var t=0; t<TURBO_TIME+PREDICTION_TIME-10; t++) {
    throttle = (t <= TURBO_TIME) ? 1 : 0;
    exports.updateSimCar(track, car,
        throttle * exports.turboFactor(tick + t + 2, turbo),
        {predictionMode: true});

    if (Math.abs(car.angle) > exports.physics.MAX_ANGLE-2) { return false; }
  }
  return true;
};

exports.turboFactor = function(tick, turbo) {
  if (!turbo) { return 1; }
  return tick <= turbo.startTick + turbo.turboDurationTicks
      ? turbo.turboFactor
      : 1;
};

exports.carToLog = function(c) {
  return [c.tick, c.pieceIndex, c.inPieceDistance, c.endLaneIndex, c.turboFactor, c.a, c.speed, c.angle, c.angleSpeed, c.angleA];
};

exports.markCrash = function(car) {
  exports.crashMarks[car.pieceIndex] = true;
};

exports.crashMarks = {};
exports.fitness = _findSumError;

function _shouldAddTick(car, track) {
  return car.pieceIndex === track.turboIndex
      || track.switchDirection((car.pieceIndex + 1) % track.length, car.endLaneIndex);
}

function _findSumError(car0, data, track, physics, logData) {
  var totalErr = 0,
      car = _.clone(car0),
      msg = '', err, real;

  for (var i=1; i<data.length; i++) {
    real = data[i];
    exports.updateSimCar(track, car, real.throttle, {
      calibrationMode: true,
      physics: physics,
      car: data[i-1].car
    });
    err = Math.abs(real.angle - car.angle);

    // Check edge cases and normalize an error by angle
    if (!real.angle && !car.angle) {
      err = 0;
    } else if (!real.angle || !car.angle) {
      err = 1;
    } else {
      err = Math.abs(real.angle - car.angle) / Math.sqrt(Math.abs(real.angle));
    }
    totalErr += Math.abs(err);

    if (logData) {
      msg += real.angle + ','
          + car.angle + ','
          + (real.angle - car.angle) + ','
          + real.car.speed + ','
          + car.speed + '\n';
    }
  }
  if (msg) { Log.logTxt('av', msg, 'csv'); }
  return totalErr / data.length;
}

// Finds max angle error by simulating car run with given physics constants
function _findMaxError(car0, data, track, physics) {
  var maxErr = 0,
      car = _.clone(car0),
      err, msg = '';

  for (var i=1; i<data.length; i++) {
    exports.updateSimCar(track, car, data[i].throttle, {calibrationMode: true, physics: physics});
    err = Math.abs(data[i].angle) - Math.abs(car.angle);
    if (Math.abs(err) > Math.abs(maxErr)) { maxErr = err; }
  }
  return maxErr;
}
