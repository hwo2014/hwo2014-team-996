var _ = require('lodash'),
    Const = require('./constants');

var track, sections;

exports.SWITCH_RADIUS = false;

exports.init = function(race) {
  track = race.track;
  _.each(track.pieces, function(pc, index) { pc.index = index; });

  exports.track = track;
  exports.length = track.pieces.length;
  exports.sections = sections = findSections(race.track);
  exports.turboIndex = _.max(_.range(0, track.pieces.length), runDistanceAhead);
};

/**
 * Returns piece radius for bended piece WRT the lane index
 * TODO: consider both lanes
 */
exports.pieceRadius = function(piece, startLaneIndex, endLaneIndex) {
  piece = _.isNumber(piece) ? track.pieces[piece] : piece;

  if (startLaneIndex === endLaneIndex) {
    return piece.angle
        ? piece.radius + (piece.angle < 0 ? 1 : -1) * exports.laneDistance(endLaneIndex)
        : Const.INFINITY;
  }

  if (piece.angle) {
    var startLaneDist = (piece.angle < 0 ? 1 : -1) * exports.laneDistance(startLaneIndex),
        endLaneDist = (piece.angle < 0 ? 1 : -1) * exports.laneDistance(endLaneIndex);
    return piece.radius + Math.min(startLaneDist, endLaneDist);
  } else {
    var angle = exports.pieceAngle(piece, startLaneIndex, endLaneIndex, 0);
    return exports.pieceLength(piece, startLaneIndex, endLaneIndex) * 90 / (Math.PI * angle);
  }
};

/**
 * Returns length for both straight and bended pieces
 */
exports.pieceLength = function(piece, startLaneIndex, endLaneIndex) {
  piece = _.isNumber(piece) ? track.pieces[piece] : piece;

  // If switch is happening, this is a whole different story
  if (piece.switch && startLaneIndex !== endLaneIndex) {
    var interval = exports.laneDistance(endLaneIndex) - exports.laneDistance(startLaneIndex);
    // Approximate straight piece as a hypotenuse
    if (piece.length) { return Math.sqrt(piece.length*piece.length + interval*interval); }

    // Nightmare case: switch on the bend
    var lenStart = Math.PI
        * exports.pieceRadius(piece, startLaneIndex, startLaneIndex)
        * Math.abs(piece.angle) / 180;
    var lenEnd = Math.PI
        * exports.pieceRadius(piece, endLaneIndex, endLaneIndex)
        * Math.abs(piece.angle) / 180;

    return (lenStart + lenEnd) / 2;
  }

  if (piece.length) { return piece.length; }
  return Math.PI * exports.pieceRadius(piece, endLaneIndex, endLaneIndex) * Math.abs(piece.angle) / 180;
};

exports.pieceAngle = function(piece, startLaneIndex, endLaneIndex, position) {
  piece = _.isNumber(piece) ? track.pieces[piece] : piece;
  if (startLaneIndex === endLaneIndex || piece.angle) { return piece.angle; }

  // Rough approximation for a straight section with switch
  var length = exports.pieceLength(piece, startLaneIndex, endLaneIndex),
      interval = exports.laneDistance(endLaneIndex) - exports.laneDistance(startLaneIndex),
      angle = Math.asin(Math.abs(interval) / length) * 180 / Math.PI,
      secondTurn = position > length / 2;

  return angle * (endLaneIndex - startLaneIndex) * (secondTurn ? -1 : 1);
};

exports.laneDistance = function(laneIndex) {
  var lane = _.find(track.lanes, function(lane) { return lane.index === laneIndex; });
  return lane && lane.distanceFromCenter || 0;
};

exports.switchDirection = _.memoize(switchDirection, function(a, b) { return a + '_' + b; });
exports.nextSections = _.memoize(nextSections);

exports.remainingDistanceInSection = function(section, pieceIndex, laneIndex, inPieceDistance) {
  var piece = track.pieces[pieceIndex],
      piecesDist = exports.pieceLength(piece, laneIndex, laneIndex) - inPieceDistance;

  if (pieceIndex === section.endIndex) { return piecesDist; }

  for (var i=pieceIndex+1; i<=section.endIndex; i++) {
    piecesDist += exports.pieceLength(i, laneIndex, laneIndex);
  }
  return piecesDist;
};



/**
 * Select switch direction: -1 (left), 1 (right) or 0 (no switch)
 */
function switchDirection(index, laneIndex) {
  var firstSwitch, secondSwitch, lanes,
      minValue = Const.INFINITY,
      maxRadius = 0,
      switchTo = 0;

  // Find next switch
  firstSwitch = nextSwitch(index) || nextSwitch();

  // If this happens we most likely have one lane
  if (!firstSwitch) { return 0; }

  // Find switch after the next
  secondSwitch = nextSwitch(firstSwitch) || nextSwitch();

  // Find lane with min parameter between switches.
  _.each(track.lanes, function(lane) {
    if (Math.abs(lane.index - laneIndex) > 1) { return; }

    var value = _.reduce(track.pieces, function(memo, pc) {
      if (firstSwitch < secondSwitch) {
        return (pc.index > firstSwitch && pc.index < secondSwitch ? laneValue(pc, lane.index, lane.index) : 0) + memo;
      } else {
        return ((pc.index > firstSwitch && pc.index < track.pieces.length)
            || (pc.index > 0 && pc.index < secondSwitch) ? laneValue(pc, lane.index, lane.index) : 0) + memo;
      }
    }, 0);

    // Edge case, equal lanes - do not change lane
    if (value === minValue) { switchTo = laneIndex; }

    if (value < minValue) {
      minValue = value;
      switchTo = lane.index;
    }
  });

  // No change
  if (switchTo === laneIndex) { return 0; }
  return switchTo < laneIndex ? -1 : 1;
}

function nextSwitch(index) {
  for (var i=(index === undefined ? 0 : index + 1); i<track.pieces.length; i++) {
    if (track.pieces[i].switch
        && (!track.pieces[i].angle || !sharpBend(track.pieces[i]))) { return i; }
  }
}

function sharpBend(piece) {
  return piece.angle && Math.abs(piece.angle / piece.radius) > 0.5;
}

function laneValue(pc, laneIndex) {
  return exports.SWITCH_RADIUS
      ? -exports.pieceRadius(pc, laneIndex, laneIndex)
      : exports.pieceLength(pc, laneIndex, laneIndex);
}

/***************************************************************************************************
 Sections
***************************************************************************************************/
function findSections(track) {
  var result = [], section, length = 0;

  function laneLengths(piece, lengths) {
    return _.map(track.lanes, function(lane) {
      return (lengths && lengths[lane.index] || 0) + exports.pieceLength(piece, lane.index, lane.index);
    });
  }

  function newSection(piece, index, direction) {
    return {
      sectionIndex: result.length,
      startIndex: index,
      direction: direction,
      angle: piece.angle || 0,
      lengths: laneLengths(piece),

      maxAngle: 0,
      maxAngleSpeed: 0
    };
  }

  _.each(track.pieces, function(piece, index) {
    var direction = piece.length ? 0 : piece.angle < 0 ? -1 : 1,   // "0" - forward, "-1" - left, "1" - right
        radiuses = _.map(track.lanes, function(lane) {
          return piece.length ? Const.INFINITY : exports.pieceRadius(piece, lane.index, lane.index);
        });

    if (!section) {
      // Create new section
      section = newSection(piece, index, direction);
      section.radiuses = radiuses;
    } else if (direction === section.direction && radiuses[0] === section.radiuses[0]) {
      // Continue existing section with the same direction
      section.lengths = laneLengths(piece, section.lengths);
      section.angle += piece.angle || 0;
    } else {
      // Finish existing section and create new one
      section.endIndex = index - 1;
      result.push(_.clone(section));
      section = newSection(piece, index, direction);
      section.radiuses = radiuses;
    }
  });
  section.endIndex = track.pieces.length - 1;
  result.push(_.clone(section));
  return result;
}

function nextSections(pieceIndex) {
  var curSection = _.find(sections, function(section) {
    return pieceIndex >= section.startIndex && pieceIndex <= section.endIndex;
  });

  return {
    current: curSection,
    next: curSection.sectionIndex + 1 < sections.length
        ? sections[curSection.sectionIndex + 1]
        : sections[0]
  };
}

// Returns length of a more or less straight distance ahead
function runDistanceAhead(index) {
  var track = nextSections(index);

  if (track.current.direction) { return 0; }
  return track.current.lengths[0] + (track.next.direction ? 0 : track.next.lengths[0]);
}
