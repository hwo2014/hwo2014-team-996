exports.missingTicks = function(currentTick, tick, position) {
  if (tick - currentTick > 1) { console.log(tick + ': ALARM! Server missed ticks: ' + (tick - currentTick - 1)); }

  var mTicks = Math.max(tick - position.prevCommandTick - 1, 0);
  if (mTicks) { console.log(tick + ': ALARM! Car missed ticks: ' + mTicks); }
  return mTicks;
};

// Return time difference in microseconds
exports.benchmark = function(tStart) {
  var tEnd = process.hrtime(tStart);
  return (tEnd[0] * 1e9 + tEnd[1]) / 1e3;
};
